package com.example.mapsdirectiondemo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mapsdirectiondemo.R
import com.example.mapsdirectiondemo.adapters.TaskListAdapter
import com.example.mapsdirectiondemo.model.response.ApiResponse
import com.example.mapsdirectiondemo.model.response.Task
import com.example.mapsdirectiondemo.retrofit2.CommonParams
import com.example.mapsdirectiondemo.retrofit2.RestClient
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var rvTaskList: RecyclerView
    lateinit var taskListAdapter: TaskListAdapter
    lateinit var llLoading: LinearLayoutCompat
    val allTaskList = ArrayList<Task>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvTaskList = findViewById(R.id.rvTaskList)
        llLoading = findViewById(R.id.llLoading)
        taskListAdapter = TaskListAdapter(this, allTaskList)
        rvTaskList.layoutManager = LinearLayoutManager(this)
        rvTaskList.adapter = taskListAdapter
        getTasksList()

    }

    private fun getTasksList(){
        llLoading.visibility = View.VISIBLE
        val params = CommonParams.Builder().apply {
//            add("access_token", "186b6682b20159131c162e3e1114274f18e3c3f92ac10b22591a02c2c9fb")
            add("access_token", "062163d7b045530a1c162e3e1616224619e5c1f22bc10b22551a02c7cffc") // mine
//            add("date", "2020-10-30")
            add("date", "2020-11-27")
            add("version", "v2")
            add("android_channel_id", "home_refresh_swipe")
        }.build()
        RestClient.getApiInterface().viewTaskForDate(params.map)
            .enqueue(object: retrofit2.Callback<ApiResponse> {
                override fun onResponse(call: Call<ApiResponse>, response: Response<ApiResponse>) {
                    llLoading.visibility = View.GONE
                    allTaskList.clear()
                    val todaysTasks = response.body()?.data?.tasks
                    if(todaysTasks == null || todaysTasks.size == 0) {
                        Toast.makeText(this@MainActivity, "No Tasks available", Toast.LENGTH_LONG).show()
                        return
                    }
                    for(tasks in todaysTasks) {
                        allTaskList.addAll(tasks)
                    }
                    taskListAdapter.updateList(allTaskList)
                }

                override fun onFailure(call: Call<ApiResponse>, t: Throwable) {
                    llLoading.visibility = View.GONE
                    t.printStackTrace()
                }

            })
    }
}