package com.example.mapsdirectiondemo.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.LinearLayoutCompat
import com.example.mapsdirectiondemo.R
import com.example.mapsdirectiondemo.fragments.MapFragment
import com.example.mapsdirectiondemo.fragments.TaskDetailsFragment
import com.example.mapsdirectiondemo.model.response.Task
import com.google.android.material.tabs.TabLayout

class TaskDetailsActivity : AppCompatActivity() {

    private lateinit var tabLayout: TabLayout
    private lateinit var taskDetails: Task
    lateinit var llLoading: LinearLayoutCompat
    private lateinit var taskDetailsFragment: TaskDetailsFragment
    private lateinit var mapFragment: MapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.task_details_activity)
        tabLayout = findViewById(R.id.tabLayout)
        llLoading = findViewById(R.id.llLoading)
        taskDetails = intent.getParcelableExtra("taskDetails")!!
        taskDetailsFragment = TaskDetailsFragment.newInstance(taskDetails)
        mapFragment = MapFragment(taskDetails)
        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, taskDetailsFragment).commitNow()
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if(tab!=null)
                    when(tab.position){
                        0 -> supportFragmentManager.beginTransaction().replace(R.id.frameLayout, taskDetailsFragment).commitNow()
                        1 -> supportFragmentManager.beginTransaction().replace(R.id.frameLayout, mapFragment).commitNow()
                    }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })
    }

    fun showLoading() {
        llLoading.visibility = View.VISIBLE
    }

    fun hideLoading() {
        llLoading.visibility = View.GONE
    }
}