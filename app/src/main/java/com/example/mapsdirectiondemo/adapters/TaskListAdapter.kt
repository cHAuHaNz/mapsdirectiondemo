package com.example.mapsdirectiondemo.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mapsdirectiondemo.R
import com.example.mapsdirectiondemo.activities.TaskDetailsActivity
import com.example.mapsdirectiondemo.model.response.Task

class TaskListAdapter(private val context: Context, private var taskList: ArrayList<Task>): RecyclerView.Adapter<TaskListAdapter.TaskViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListAdapter.TaskViewHolder {
        return TaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.task_list_row_item, parent, false))
    }

    override fun onBindViewHolder(holder: TaskListAdapter.TaskViewHolder, position: Int) {
        holder.bind(taskList[holder.adapterPosition])
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    fun updateList(list: ArrayList<Task>){
        taskList = list
        notifyDataSetChanged()
    }

    inner class TaskViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(task: Task) {
            itemView.findViewById<AppCompatTextView>(R.id.tvAddress).text = task.jobPickupAddress
            itemView.findViewById<AppCompatTextView>(R.id.tvTaskDateTime).text = task.jobTime
            itemView.setOnClickListener{
                context.startActivity(Intent(context, TaskDetailsActivity::class.java).apply {
                    putExtra("taskDetails", task)
                })
            }
        }
    }
}


