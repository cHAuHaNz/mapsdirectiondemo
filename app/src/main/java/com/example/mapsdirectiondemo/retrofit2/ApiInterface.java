package com.example.mapsdirectiondemo.retrofit2;

import com.example.mapsdirectiondemo.model.response.ApiResponse;
import com.example.mapsdirectiondemo.model.response.RoutesResponse;

import java.util.Map;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("/view_tasks_for_date")
    Call<ApiResponse> viewTaskForDate(@FieldMap Map<String, String> map);

    @GET("/route")
    Call<RoutesResponse> getRoutes(@QueryMap Map<String, String> map);

}


