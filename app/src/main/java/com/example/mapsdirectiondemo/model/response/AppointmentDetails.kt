package com.example.mapsdirectiondemo.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AppointmentDetails(
    private val image: String?,
    private val longitude: String?,
    private val appointment_field_data_id: String?,
    private val latitude: String?,
    private val notes: String?,
    private val creation_datetime: String?,
    @Transient
    private val isUploadable: Boolean,
    @Transient
    private val isUploading: Boolean,
): Parcelable