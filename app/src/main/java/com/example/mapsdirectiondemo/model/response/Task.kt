package com.example.mapsdirectiondemo.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlin.collections.ArrayList

@Parcelize
data class Task(
@SerializedName("customer_email")
var customerEmail: String? = null,
@SerializedName("customer_phone")
var customerPhone: String? = null,
@SerializedName("customer_username")
var customerUsername: String? = null,
@SerializedName("job_address")
var jobAddress: String? = null,
@SerializedName("job_delivery_datetime")
var jobDeliveryDatetime: String? = null,
@SerializedName("job_description")
var jobDescription: String? = null,
@SerializedName("pickup_delivery_relationship")
var pickupDeliveryRelationship: String? = null,
@SerializedName("has_field_input")
var hasFieldInput: String? = null,
@SerializedName("payment_status")
var paymentStatus: String? = null,
@SerializedName("fare")
var fare: String? = null,
@SerializedName("total_distance_travelled")
var totalDistanceTravelled: String? = null,
@SerializedName("total_time")
var totalTime: String? = null,
@SerializedName("vertical")
var vertical: String? = null,
@SerializedName("task_status_text")
var taskStatusText: String? = null,
@SerializedName("task_status_color")
var taskStatusColor: String? = null,
@SerializedName("job_pickup_address")
var jobPickupAddress: String? = null,
@SerializedName("job_pickup_datetime")
var jobPickupDatetime: String? = null,
@SerializedName("job_pickup_latitude")
var jobPickupLatitude: String? = null,
@SerializedName("job_pickup_longitude")
var jobPickupLongitude: String? = null,
@SerializedName("job_pickup_name")
var jobPickupName: String? = null,
@SerializedName("job_pickup_phone")
var jobPickupPhone: String? = null,
@SerializedName("job_status")
var jobStatus: Int = 0,
@SerializedName("job_time")
var jobTime: String? = null,
@SerializedName("job_type")
var jobType: Int = 0,
@SerializedName("session_id")
var sessionId: String? = null,
@SerializedName("driver_job_total")
var driverJobTotal: String? = null,
@SerializedName("customer_job_total")
var customerJobTotal: String? = null,
@SerializedName("job_latitude")
var jobLatitude: String? = null,
@SerializedName("job_longitude")
var jobLongitude: String? = null,
@SerializedName("order_id")
var orderId: String? = null,
@SerializedName("name")
var name: String? = null,
@SerializedName("job_pickup_email")
var jobPickupEmail: String? = null,
@SerializedName("job_id")
var jobId: Long = 0,
@SerializedName("delivery_job_id")
var deliveryJobId: Long = 0,
@SerializedName("pickup_job_id")
var pickupJobId: Long = 0,
@SerializedName("has_pickup")
var hasPickup: Int = 0,
@SerializedName("has_delivery")
var hasDelivery: Int = 0,
@SerializedName("related_job_type")
var relatedJobType: Int = 0,
@SerializedName("related_job_count")
var relatedJobCount: Int = 0,
@SerializedName("is_routed")
var isRouted: Int = 0,
@SerializedName("created_by")
var createdBy: Int = 0,
@SerializedName("linkedStatus")
var linkedStatus: Int = 0,
@SerializedName("is_otp_generated")
var isOtpGenerated: Int = 0,
@SerializedName("customer_rating")
var customerRating: Float = 0f,
@SerializedName("isAnimate")
var isAnimate: Boolean = false,
@SerializedName("isPaymentDone")
var isPaymentDone: Boolean = false,
@SerializedName("related_job_indexes")
var relatedJobIndexes: ArrayList<Int>? = null,
@SerializedName("job_details_by_fleet")
var jobDetailsByFleet: ArrayList<AppointmentDetails>? = null,
@SerializedName("acknowledged_images")
var acknowledgedImages: ArrayList<AcknowledgedImage>? = null,
@SerializedName("connectedTask")
var connectedTask: ArrayList<String>? = null,
@SerializedName("task_history")
var taskHistory: ArrayList<TaskHistory>? = null,
@SerializedName("fleet_id")
var fleetId: String? = null,
@SerializedName("tags")
var tags: String? = null,
@SerializedName("auto_assignment")
var autoAssignment: Int = 0,
@SerializedName("other_dates_job_count")
var otherDatesJobCount: Int = 0,
@SerializedName("barcode")
var barcode: String? = null,
@SerializedName("isBarcodeVerified")
var isBarcodeVerified: Int = 0,
@SerializedName("routeNumber")
var routeNumber: Int = 0,
@SerializedName("isLastRouteTask")
var isLastRouteTask: Int = 0,
@SerializedName("parentRouteNumber")
var parentRouteNumber: Int = 0,
@SerializedName("isFilterEnabled")
var isFilterEnabled: Int = 0,
@SerializedName("route_id")
var routeId: String? = null,
@SerializedName("job_capacity")
var jobCapacity: Int = 0,
@SerializedName("user_id")
var userId: String? = null
): Parcelable