package com.example.mapsdirectiondemo.model.response

import com.google.gson.annotations.SerializedName

class ApiResponse {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: Long = 0

    @SerializedName("data")
    var data: Data? = null
}