package com.example.mapsdirectiondemo.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class AcknowledgedImage(
    private val creation_datetime: String?,
    private val image: String?,
    private val jobs_image_id: String?,
    private val longitude: String?,
    private val latitude: String?,
    private val datetime: String?,
    private val notes: String?,
    private val acknowledge_type: String?,
): Parcelable