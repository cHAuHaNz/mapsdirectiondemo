package com.example.mapsdirectiondemo.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

class Data {
    @SerializedName("routed_tasks")
    var routedTasks: ArrayList<Any>? = null

    @SerializedName("new_tasks")
    var newTasks: ArrayList<Any>? = null

    @SerializedName("polylines")
    var polylines: ArrayList<Any>? = null

    @SerializedName("tasks")
    var tasks: ArrayList<ArrayList<Task>>? = null

    @SerializedName("routed_tasks_order")
    var routedTasksOrder: ArrayList<Any>? = null

    @SerializedName("total_new_task_count")
    var totalNewTaskCount: Long = 0
}