package com.example.mapsdirectiondemo.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TaskHistory : Parcelable {
    @SerializedName("id")
    var id: Long = 0
    @SerializedName("job_id")
    var jobID: Long = 0
    @SerializedName("fleet_id")
    var fleetID: Long? = null
    @SerializedName("fleet_name")
    var fleetName: String? = null
    @SerializedName("latitude")
    var latitude: String? = null
    @SerializedName("longitude")
    var longitude: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("extra_fields")
    var extraFields: Any? = null
    @SerializedName("creation_datetime")
    var creationDatetime: String? = null
    @SerializedName("creation_date")
    var creationDate: String? = null
    @SerializedName("label_description")
    var labelDescription: String? = null
}