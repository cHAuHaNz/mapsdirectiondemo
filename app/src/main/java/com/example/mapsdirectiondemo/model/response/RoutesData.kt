package com.example.mapsdirectiondemo.model.response

import com.google.gson.annotations.SerializedName

class RoutesData {
    @SerializedName("points")
    var routePoints: ArrayList<RoutePoint>? = null
    @SerializedName("polylines")
    var polyLines: ArrayList<String>? = null
    data class RoutePoint(var lat: Double, var lng: Double)
}