package com.example.mapsdirectiondemo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.mapsdirectiondemo.R
import com.example.mapsdirectiondemo.model.response.Task

class TaskDetailsFragment : Fragment() {

    private lateinit var taskDetails: Task

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            taskDetails = it.getParcelable("taskDetails")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_task_details, container, false)
        val tvTimeAndType = v.findViewById<TextView>(R.id.tvTimeAndType)
        val tvTaskStatus = v.findViewById<TextView>(R.id.tvTaskStatus)
        val tvName = v.findViewById<TextView>(R.id.tvName)
        val tvAddress = v.findViewById<TextView>(R.id.tvAddress)
        val tvDescription = v.findViewById<TextView>(R.id.tvDescription)
        val tvOrderId = v.findViewById<TextView>(R.id.tv_order_id)
        tvTimeAndType.text = taskDetails.jobTime
        tvTaskStatus.text = getJobStatus(taskDetails.jobStatus)
        tvName.text = taskDetails.customerUsername
        tvAddress .text = taskDetails.jobAddress
        tvDescription .text = taskDetails.jobDescription
        tvOrderId.text = taskDetails.orderId
        return v
    }

    companion object {
        @JvmStatic
        fun newInstance(taskDetails: Task) =
            TaskDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("taskDetails", taskDetails)
                }
            }
    }

    private fun getJobStatus(status: Int): String {
        when(status) {
            6 -> return "UNASSIGNED"
            -1 -> return "NONE"
            0 -> return "ASSIGNED"
            1 -> return "STARTED"
            2 -> return "SUCCESSFUL"
            3 -> return "FAILED"
            4 -> return "ARRIVED"
            5 -> return "PARTIAL"
            7 -> return "ACCEPTED"
            7 -> return "ACKNOWLEDGED"
            8 -> return "DECLINED"
            9 -> return "CANCELED"
            11 -> return "IGNORED"
            12 -> return "OK"
            else -> return ""
        }
    }
}