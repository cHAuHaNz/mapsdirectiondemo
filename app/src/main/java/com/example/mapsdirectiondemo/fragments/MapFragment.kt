package com.example.mapsdirectiondemo.fragments

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.mapsdirectiondemo.R
import com.example.mapsdirectiondemo.activities.TaskDetailsActivity
import com.example.mapsdirectiondemo.model.response.RoutesResponse
import com.example.mapsdirectiondemo.model.response.Task
import com.example.mapsdirectiondemo.retrofit2.CommonParams
import com.example.mapsdirectiondemo.retrofit2.RoutesRestClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import retrofit2.Call
import retrofit2.Response


class MapFragment(private val taskDetails: Task) : Fragment(), GoogleMap.OnMapLongClickListener {

    private var googleMap: GoogleMap? = null
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null
    private var marker: Marker? = null
    private var carMarker: Marker? = null
    private lateinit var taskDetailsActivity: TaskDetailsActivity
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var btnStart: AppCompatButton
    private lateinit var btnReset: AppCompatButton
    private var pathLatLng: ArrayList<LatLng> = ArrayList()
    private var movingCarThread: Thread? = null

    private val callback = OnMapReadyCallback { map ->
        if (map != null) {
            googleMap = map
            googleMap?.setOnMapLongClickListener(this)
            getLocationPermission()
            updateLocationUI()
            marker = googleMap?.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            taskDetails.jobLatitude?.toDouble() ?: 0.0,
                            taskDetails.jobLongitude?.toDouble() ?: 0.0
                        )
                    )
                    .title("Pickup Point")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_green))
            )
            getDeviceLocationAndMakePath()
        }
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        taskDetailsActivity = activity as TaskDetailsActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(taskDetailsActivity)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_map, container, false)
        btnReset = v.findViewById(R.id.btnReset)
        btnStart = v.findViewById(R.id.btnStart)

        btnStart.setOnClickListener {
            btnStart.isEnabled = false
            btnReset.isEnabled = true
            movingCarThread = Thread {
                for (latLng in pathLatLng) {
                    val thisThread = Thread.currentThread()
                    if(movingCarThread != thisThread)
                        break
                    taskDetailsActivity.runOnUiThread {
                        carMarker?.remove()
                        carMarker = googleMap?.addMarker(
                            MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        )
                        googleMap?.moveCamera(
                            CameraUpdateFactory.newLatLng(latLng)
                        )
                    }
                    Thread.sleep(400)
                }
            }
            movingCarThread!!.start()
        }

        btnReset.setOnClickListener{ resetMarkers() }

        return v
    }

    private fun resetMarkers() {
        movingCarThread = null
        carMarker?.remove()
        btnStart.isEnabled = true
        btnReset.isEnabled = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                taskDetailsActivity.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                taskDetailsActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                101
            )
        }
    }

    private fun updateLocationUI() {
        if (googleMap == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                googleMap?.isMyLocationEnabled = true
                googleMap?.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                googleMap?.isMyLocationEnabled = false
                googleMap?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getDeviceLocationAndMakePath() {
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(taskDetailsActivity) { task ->
                    if (task.isSuccessful) {
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            googleMap?.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    LatLng(
                                        lastKnownLocation!!.latitude,
                                        lastKnownLocation!!.longitude
                                    ), 12f
                                )
                            )
                            googleMap?.addMarker(
                                MarkerOptions()
                                    .position(
                                        LatLng(
                                            lastKnownLocation!!.latitude,
                                            lastKnownLocation!!.longitude
                                        )
                                    )
                                    .title("Starting Point")
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_red))
                            )
                            makeDirectionPath()
                        }
                    } else {
                        googleMap?.moveCamera(
                            CameraUpdateFactory
                                .newLatLngZoom(LatLng(0.0, 0.0), 12f)
                        )
                        googleMap?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    override fun onMapLongClick(point: LatLng?) {
        googleMap?.clear()
        resetMarkers()
        googleMap?.animateCamera(CameraUpdateFactory.newLatLng(point!!))
        marker = googleMap?.addMarker(
            MarkerOptions()
                .position(point!!)
                .title("Pickup Point")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_pin))
        )
        getDeviceLocationAndMakePath()
    }

    private fun makeDirectionPath() {
        if (lastKnownLocation == null)
            return
        taskDetailsActivity.showLoading()
        val params = CommonParams.Builder().apply {
            add("start", "${lastKnownLocation?.latitude},${lastKnownLocation?.longitude}")
            add("end", "${marker?.position?.latitude},${marker?.position?.longitude}")
            add("key", taskDetailsActivity.getString(R.string.google_maps_key))
        }.build()
        RoutesRestClient.getApiInterface().getRoutes(params.map)
            .enqueue(object : retrofit2.Callback<RoutesResponse> {
                override fun onResponse(
                    call: Call<RoutesResponse>,
                    response: Response<RoutesResponse>
                ) {
                    var index = 0
                    val polyLines = response.body()?.data?.polyLines
                    pathLatLng.clear()
                    if (polyLines != null)
                        for (encoded in polyLines) {
                            val points: ArrayList<LatLng> = decodePolyline(encoded)
                            if (points.isEmpty()) continue
                            if (index > 9) {
                                index = 0
                            }
                            val polylineOptions = PolylineOptions().color(Color.parseColor("#007AFF"))
                            for (latLng in points) {
                                polylineOptions.add(latLng)
                                pathLatLng.add(latLng)
                            }
                            googleMap!!.addPolyline(polylineOptions)
                            index++
                        }
                    taskDetailsActivity.hideLoading()
                }

                override fun onFailure(call: Call<RoutesResponse>, t: Throwable) {
                    AlertDialog.Builder(taskDetailsActivity).setMessage(t.localizedMessage)
                        .setCancelable(false).setNegativeButton("Ok", null).show()
                    taskDetailsActivity.hideLoading()
                }
            })
    }

    private fun decodePolyline(encoded: String): ArrayList<LatLng> {
        val poly: ArrayList<LatLng> = ArrayList()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }
        return poly
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            101 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                    updateLocationUI()
                }
            }
        }
    }
}